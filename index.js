const points = [
  { color: [0xff, 0x00, 0x00], position: [0, 0] },
  { color: [0x00, 0xff, 0x00], position: [1, 0] },
  { color: [0x00, 0x00, 0xff], position: [1, 1] },
  { color: [0xff, 0xa5, 0x00], position: [0, 1] }
];

document.addEventListener("mousemove", (event) => {
  let x = event.clientX / window.innerWidth;
  let y = event.clientY / window.innerHeight;

  process(x, y);
});

let listen = () => {
  window.addEventListener("deviceorientation", (event) => {
    // console.log(event.alpha, event.beta, event.gamma);

    let x = (1 + event.gamma / 45) * 0.5;
    let y = (1 + event.beta / 45) * 0.5;

    process(x, y);
  });
}

if (typeof DeviceOrientationEvent.requestPermission === 'function') {
  document.querySelector('button').addEventListener('click', (e) => {
    e.preventDefault();

    DeviceOrientationEvent.requestPermission()
      .then(permissionState => {
        if (permissionState === 'granted') {
          listen();
        }
      })
      .catch(console.error);
  });
} else {
  listen();
}


function process(x, y) {
  let weights = points
    .map((point) => (point.position[0] - x) ** 2 + (point.position[1] - y) ** 2)
    .map((dist) => 1 / Math.sqrt(dist));

  let weightsSum = weights.reduce((sum, value) => sum + value, 0);

  let outColor = [0, 0, 0];

  points.forEach((point, index) => {
    let weight = weights[index] / weightsSum;

    for (let i = 0; i < 3; i++) {
      outColor[i] += point.color[i] * weight;
    }
  });

  document.body.style.setProperty(
    "--backcolor",
    `#${outColor
        .map((comp) => Math.round(comp).toString(16).padStart(2, "0"))
        .join("")}`
  );
}
